package com.serino.adapterpatterndemo.adapter

import android.util.Log
import com.serino.adapterpatterndemo.device.Radio

class RadioToRemote(private val radio: Radio): RemoteAdapter {

	override fun switchOn() {
		Log.d("DLog RadioToRemote", "Radio is switched ON")
		radio.open()
	}

	override fun switchOff() {
		Log.d("DLog RadioToRemote", "Radio is switched OFF")
		radio.close()
	}

	override fun increaseVolume() {
		Log.d("DLog RadioToRemote", "Radio volume increased")
		radio.incVol()
	}

	override fun decreaseVolume() {
		Log.d("DLog RadioToRemote", "Radio volume increased")
		radio.decVol()
	}
}
