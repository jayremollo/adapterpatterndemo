package com.serino.adapterpatterndemo.device

import android.util.Log

class Television {

	private var isOn: Boolean = false
	private var volume: Int = 0

	fun on() {
		isOn = true
		Log.d("DLog Television", "Television is ON")
	}

	fun off() {
		isOn = false
		Log.d("DLog Television", "Television is OFF")
	}

	fun volumeUp() {
		if (volume < 10) volume += 1
		Log.d("DLog Television", "Television volume up to $volume")
	}

	fun volumeDown() {
		if (volume > 0) volume -= 1
		Log.d("DLog Television", "Television volume down to $volume")
	}
}
