package com.serino.adapterpatterndemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.serino.adapterpatterndemo.adapter.RadioToRemote
import com.serino.adapterpatterndemo.adapter.RemoteAdapter
import com.serino.adapterpatterndemo.adapter.TelevisionToRemote
import com.serino.adapterpatterndemo.databinding.ActivityMainBinding
import com.serino.adapterpatterndemo.device.Radio
import com.serino.adapterpatterndemo.device.Television

class MainActivity : AppCompatActivity() {

	private lateinit var binding: ActivityMainBinding
	private lateinit var remote: RemoteAdapter
	private lateinit var televisionAdapter: RemoteAdapter
	private lateinit var radioAdapter: RemoteAdapter

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityMainBinding.inflate(layoutInflater)
		setContentView(binding.root)

		televisionAdapter = TelevisionToRemote(Television())
		radioAdapter = RadioToRemote(Radio())
		setRemote(televisionAdapter)

		initViews()
	}

	private fun initViews() {
		with(binding) {
			btnTelevision.setOnClickListener {
				setRemote(televisionAdapter)
			}
			btnRadio.setOnClickListener {
				setRemote(radioAdapter)
			}
			btnOn.setOnClickListener {
				Log.d("DLog MainActivity", "remote switch on")
				remote.switchOn()
			}
			btnOff.setOnClickListener {
				Log.d("DLog MainActivity", "remote switch off")
				remote.switchOff()
			}
			btnUp.setOnClickListener {
				Log.d("DLog MainActivity", "remote volume up")
				remote.increaseVolume()
			}
			btnDown.setOnClickListener {
				Log.d("DLog MainActivity", "remote volume down")
				remote.decreaseVolume()
			}
		}
	}

	private fun setRemote(remote: RemoteAdapter) {
		this.remote = remote
		Log.d("DLog MainActivity", "Currently using ${remote.javaClass.simpleName}")
	}
}
