package com.serino.adapterpatterndemo.adapter

interface RemoteAdapter {
	fun switchOn()
	fun switchOff()
	fun increaseVolume()
	fun decreaseVolume()
}
