package com.serino.adapterpatterndemo.adapter

import android.util.Log
import com.serino.adapterpatterndemo.device.Television

class TelevisionToRemote(private val television: Television): RemoteAdapter {

	override fun switchOn() {
		Log.d("DLog TelevisionToRemote", "Television is switched ON")
		television.on()
	}

	override fun switchOff() {
		Log.d("DLog TelevisionToRemote", "Television is switched OFF")
		television.off()
	}

	override fun increaseVolume() {
		Log.d("DLog TelevisionToRemote", "Television volume increased")
		television.volumeUp()
	}

	override fun decreaseVolume() {
		Log.d("DLog TelevisionToRemote", "Television volume decreased")
		television.volumeDown()
	}
}
