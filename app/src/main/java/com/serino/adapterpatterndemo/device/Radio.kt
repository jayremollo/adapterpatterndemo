package com.serino.adapterpatterndemo.device

import android.util.Log

class Radio {

	private var isOpen: Boolean = false
	private var vol: Int = 0

	fun open() {
		isOpen = true
		Log.d("DLog Radio", "Radio is OPEN")
	}

	fun close() {
		isOpen = false
		Log.d("DLog Radio", "Radio is CLOSED")
	}

	fun incVol() {
		if(vol < 10) vol += 1
		Log.d("DLog Radio", "Radio vol inc to $vol")
	}

	fun decVol() {
		if(vol > 0) vol -= 1
		Log.d("DLog Radio", "Radio vol dec to $vol")
	}
}
